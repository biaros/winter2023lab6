// Bianca Rossetti 2233420
import java.util.Scanner;
public class Jackpot
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome! Let's play Jackpot and see if you can win a grand total of 5$!");
		boolean stillPlaying  = true;
		int numWins = 0;
		while(stillPlaying)
		{
			Board gameBoard = new Board();
			boolean gameOver = false;
			int numOfTilesClosed = 0;
			while(!gameOver)
			{
				System.out.println(gameBoard);
				if (gameBoard.playATurn()) // if gameBoard returns true, the game is officially over
				{
					gameOver = true;
				}
				else
				{
					numOfTilesClosed++;
				}
			}
			if (numOfTilesClosed >= 7)
			{
				System.out.println("You won the jackpot!");
				numWins++;
			}
			else
			{
				System.out.println("You lost.");
			}
			System.out.println("do you still want to play? type YES to keep playing");
			if (!(scan.next().toLowerCase().equals("yes")))
			{
				stillPlaying = false;
			}
		}
		System.out.println("You have won " + numWins + " times!");
	}
}