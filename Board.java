// Bianca Rossetti 2233420
public class Board
{
	private Die dice1;
	private Die dice2;
	private boolean[] tiles;
	
	// constructor
	public Board()
	{
		this.dice1 = new Die();
		this.dice2 = new Die();
		this.tiles = new boolean[12];
	}
	
	// toString
	public String toString()
	{
		String values = "";
		for(int i = 0; i < tiles.length; i++)
		{
			if(!(tiles[i]))
			{
				values = values + (i + 1) + " ";
			}
			else
			{
				values = values + "X ";
			}
		}
		return(values);
	}
	
	// instance playATurn that plays a turn for player
	public boolean playATurn()
	{
		this.dice1.roll();
		this.dice2.roll();
		System.out.println("dice 1: " + this.dice1 + ", dice 2: " + this.dice2);
		int sumOfDice = this.dice1.getFaceValue() + this.dice2.getFaceValue();
		if (!(this.tiles[(sumOfDice - 1)]))
		{
			this.tiles[(sumOfDice - 1)] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return(false); // representing if the game has ended yet
		}
		else if (!(this.tiles[(this.dice1.getFaceValue() - 1)]))
		{
			this.tiles[(this.dice1.getFaceValue() - 1)] = true;
			System.out.println("Closing tile with the same value as die one: " + this.dice1.getFaceValue());
			return(false); // game still not over
		}
		else if (!(this.tiles[(this.dice2.getFaceValue() - 1)]))
		{
			this.tiles[(this.dice2.getFaceValue() - 1)] = true;
			System.out.println("Closing tile with the same value as die two: " + this.dice2.getFaceValue());
			return(false);
		}
		else
		{
			System.out.println("All the tiles for these values are already shut");
			return(true);
		}
	}
}