// Bianca Rossetti 2233420
import java.util.Random;
public class Die
{
	private int faceValue;
	private Random ranGen;
	
	// constructor
	public Die()
	{
		this.faceValue = 1;
		ranGen = new Random();
		
	}
	
	// getters
	public int getFaceValue()
	{
		return(this.faceValue);
	}
	
	// actions: roll the dice
	public void roll()
	{
		this.faceValue = ranGen.nextInt(6) + 1;
	}
	public String toString()
	{
		return(this.faceValue + "");
	}
}